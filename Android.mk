LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := Nextcloud
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_TAGS := optional
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_OPTIONAL_USES_LIBRARIES := org.apache.http.legacy

LOCAL_PRODUCT_MODULE := true
LOCAL_SRC_FILES := prebuilt/Nextcloud.apk

include $(BUILD_PREBUILT)
